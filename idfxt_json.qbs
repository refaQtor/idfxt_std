import qbs

CppApplication {
    type: "application" // To suppress bundle generation on Mac
    consoleApplication: true
    files: [
        "idfxt/main.cpp",
        "idfxt/JSONDataInterface.h",
        "idfxt/JSONDataInterface.cpp",
        "idfxt/cJSON.h",
        "idfxt/cJSON.c",
        "idfxt/idd-minimal-ordered.h",
        "idfxt/idd-full.h"
    ]

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }
Depends { name: "cpp" }
    cpp.cxxFlags: "-std=c++11"

}

